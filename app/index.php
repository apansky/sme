<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/app.min.css">
    <title>Peter Sagan: Superstar made in Slovakia</title>
  </head>
  <body id="swipe">
    <?php include("defs.php"); ?>
    <?php
      $lang = isset($_GET['lang']) ? $_GET['lang'] : 'sk';
      $json = file_get_contents("data/stories_" . $lang . ".json");
      $stories = json_decode($json, TRUE);
    ?>
    <div class="nav nav--initial">
      <div class="nav__inner">
        <div class="nav__left">
          <div class="nav__item nav-item--prev">
            <button type="button" class="btn btn--clear js-prev">
              <svg width="26" height="39"><use xlink:href="#icon-nav-prev"></use></svg>
            </button>
          </div>
        </div>
        <div class="nav__logo">
          <svg width="60" height="23"><use xlink:href="#icon-logo"></use></svg>
        </div>
        <div class="nav__right">
        <div class="nav__item nav-item--share">
          <button class="btn btn--clear js-share">
            <img src="assets/svg/share.svg" class="icon" alt="Share">
          </button>
          <div class="share-box">
            <div class="share-box__item">
              <button class="btn btn--social">
                <img src="assets/svg/chat.svg" alt="">
              </button>
            </div>
            <div class="share-box__item">
              <button class="btn btn--social">
                <img src="assets/svg/fb.svg" alt="">
              </button>
            </div>
            <div class="share-box__item">
              <button class="btn btn--social">
                <img src="assets/svg/twitter.svg" alt="">
              </button>
            </div>
            <div class="share-box__item">
              <button class="btn btn--social">
                <img src="assets/svg/gplus.svg" alt="">
              </button>
            </div>
            <div class="share-box__item">
              <button class="btn btn--social">
                <img src="assets/svg/link-tilted.svg" alt="">
              </button>
            </div>
          </div>
        </div>
        <div class="nav__item nav-item--next">
          <button type="button" id="pause-button" class="btn btn--clear js-next">
            <svg width="26" height="39"><use xlink:href="#icon-nav-next"></use></svg>
          </button>
        </div>
      </div>
      </div>
    </div>
    <div class="stories">
      <?php foreach($stories['data'] as $story) { ?>

          <?php switch($story['type']): case 'audio': ?>
            <div class="story <?php if ( (isset($story['starting']) || array_key_exists('starting', $story)) ) { echo 'is-active'; } ?>"
              id="<?php echo $story['slide'] ?>"
              style="background-image: url(assets/img/<?php echo $story['background'] ?>)"
              data-audio="<?php echo $story['audio'] ?>"
              data-player="<?php echo $story['slide'] ?>">
              <div class="container flex flex-column justify-end z1 fade js-alpha">
                <div class="audio-box">
                  <div class="audio-box__icon">
                    <button class="player-control js-player" data-type="<?php echo $story['type'] ?>" id="player<?php echo $story['slide'] ?>">
                      <img src="assets/svg/pause.svg" class="js-play-icon">
                    </button>
                  </div>
                  <div class="audio-box__caption">
                    <div><?php echo $story['audio-author'] ?></div>
                    <div><?php echo $story['audio-title'] ?></div>
                  </div>
                </div>
                <div class="story__content">
                  <div class="story__text">
                    <?php echo $story['text'] ?>
                  </div>
                </div>
              </div>
            </div>
          <?php break; ?>

          <?php case 'bulk': ?>
            <div class="story story--bulk" id="<?php echo $story['slide'] ?>">
              <div class="container flex items-center justify-center">
                <div class="bulk-box bulk-box--centered">
                  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                  <!-- adsence-sg-300x250 -->
                  <ins class="adsbygoogle"
                       style="display:block;width:300px;height:250px"
                       data-ad-client="ca-pub-6456364300478111"
                       data-ad-slot="6194346651"></ins>
                  <script>
                  (adsbygoogle = window.adsbygoogle || []).push({});
                  </script>
                  <?php if ( $lang == 'sk' ): ?>
                    <!-- sk ad -->
                  <?php elseif ($lang == 'en'): ?>
                    <!-- en ad -->
                  <?php endif; ?>
                </div>
              </div>
            </div>
          <?php break; ?>

          <?php case 'video': ?>
            <div class="story <?php if ( (isset($story['starting']) || array_key_exists('starting', $story)) ) { echo 'is-active'; } ?>"
              id="<?php echo $story['slide'] ?>"
              style="background-image: url(assets/img/<?php echo $story['background'] ?>)"
              data-video="<?php echo $story['video'] ?>"
              data-player="<?php echo $story['slide'] ?>">
              <div class="container flex flex-column justify-end z1 fade js-alpha">
                <div class="video-box">
                  <button class="player-control js-player" data-type="<?php echo $story['type'] ?>" id="player<?php echo $story['slide'] ?>">
                    <img src="assets/svg/pause.svg" class="js-play-icon">
                  </button>
                </div>
                <p class="video-caption"><?php echo $story['text'] ?></p>
              </div>
            </div>
          <?php break; ?>

          <?php case 'text': ?>
            <div class="story <?php if ( (isset($story['starting']) || array_key_exists('starting', $story)) ) { echo 'is-active'; } ?>"
              id="<?php echo $story['slide'] ?>"
              style="background-image: url(assets/img/<?php echo $story['background'] ?>)">
              <div class="container flex flex-column justify-end z1 fade js-alpha">
                <div class="story__content">
                  <?php if ( (isset($story['title']) || array_key_exists('title', $story)) ) { ?>
                    <div class="story__title"><?php echo $story['title'] ?></div>
                  <?php }; ?>
                  <div class="story__text">
                    <?php echo $story['text'] ?>
                  </div>
                </div>
              </div>
            </div>
          <?php break; ?>

          <?php case 'intro': ?>
            <div class="story story--intro <?php if ( (isset($story['starting']) || array_key_exists('starting', $story)) ) { echo 'is-active'; } ?>"
              id="<?php echo $story['slide'] ?>"
              style="background-image: url(assets/img/<?php echo $story['background'] ?>)">
              <div class="container flex items-center justify-center z1 fade js-alpha">
                <div class="intro text-center c-white">
                  <h1><?php echo $story['title'] ?></h1>
                  <h2><?php echo $story['text'] ?></h2>
                  <div class="mb1">
                    <?php if ( $lang == 'sk' ): ?>
                      <a href="index.php?lang=en" class="btn btn--language mr1-5">
                        <span><?php echo $story['language-switch'] ?></span>
                      </a>
                    <?php elseif ($lang == 'en'): ?>
                      <a href="index.php?lang=sk" class="btn btn--language mr1-5">
                        <span><?php echo $story['language-switch'] ?></span>
                      </a>
                    <?php endif; ?>
                    <button class="btn btn--red js-begin">
                      <?php echo $story['button'] ?>
                      <img src="assets/svg/arrow-long-right.svg" alt="" class="icon">
                    </button>
                  </div>

                  <img class="icon" src="assets/svg/audio.svg" alt="">
                  <?php if ( $lang == 'sk' ): ?>
                    <span class="c-grey f-OpenSans fs1-8">Obsahuje audio</span>
                  <?php elseif ($lang == 'en'): ?>
                    <span class="c-grey f-OpenSans fs1-8">Contains audio</span>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          <?php break; ?>

          <?php case 'graph': ?>
            <div class="story story--graph <?php if ( (isset($story['starting']) || array_key_exists('starting', $story)) ) { echo 'is-active'; } ?>"
              id="<?php echo $story['slide'] ?>"
              style="background-image: url(assets/img/15.jpg)">
              <div class="container flex flex-column z1 fade js-alpha">
                <div class="graph__title">
                  <?php echo $story['text'] ?>
                  <a class="ml1-5" href="<?php echo $value['link'] ?>"><img class="icon" src="assets/svg/link.svg" alt=""></a>
                </div>
                <div class="scroller js-prevent">
                  <div class="graph">
                    <div class="graph__lines">
                      <span class="graph__line"></span>
                      <span class="graph__line"></span>
                      <span class="graph__line"></span>
                      <span class="graph__line"></span>
                      <span class="graph__line"></span>
                    </div>
                    <?php foreach($story['values'] as $value) { ?>
                    <div class="graph__bar" style="height: <?php echo $value['height'] ?>%">
                      <div class="graph__value"><?php echo $value['count'] ?></div>
                      <div class="graph__legend"><?php echo $value['name'] ?></div>
                    </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          <?php break; ?>

          <?php case 'timeline': ?>
            <div class="story story--dark <?php if ( (isset($story['starting']) || array_key_exists('starting', $story)) ) { echo 'is-active'; } ?>"
              id="<?php echo $story['slide'] ?>"
              style="background-image: url(assets/img/<?php echo $story['background'] ?>)"
              data-timeline="timeline<?php echo $story['slide'] ?>">
              <div class="container flex flex-column z1 fade js-alpha">
                <div class="timeline-text">
                  <?php foreach($story['values'] as $value) { ?>
                    <div class="timeline-item <?php if ($value['initial'] === true) { echo 'is-active'; } ?>"
                      id="<?php echo $story['slide'] ?>-<?php echo $value['year'] ?>">
                      <div class="mt-auto">
                      <time><?php echo $value['year'] ?></time>
                      <?php foreach($value['texts'] as $text) { ?>
                        <p class=""><?php echo $text ?></p>
                      <?php } ?>
                      </div>
                    </div>
                  <?php } ?>
                </div>
                <div class="timeline js-prevent">
                  <div class="timeline__axis js-prevent">
                    <?php foreach($story['values'] as $value) { ?>
                      <div class="timeline__entry js-prevent" style="left: <?php echo $value['position'] ?>px;">
                        <div class="timeline__dot js-timeline js-prevent <?php if ($value['initial'] === true) { echo 'is-active'; } ?>" data-target="<?php echo $story['slide'] ?>-<?php echo $value['year'] ?>">
                          <span class="timeline__year js-prevent"><?php echo $value['year'] ?></span>
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          <?php break; ?>

        <?php endswitch; ?>
      <?php } ?>


      <div class="story story--outro" id="43">
        <div class="outro-overflow">
          <div class="outro-wrap">
          <div class="container outro">
            <div class="outro__header">
              <div class="outro__title">Peter Sagan: Superstar made in Slovakia</div>
              <div class="outro__share">
                <div class="outro__discuss">
                  <button class="btn btn--discuss">
                    <?php if ( $lang == 'sk' ): ?>
                      Diskusia
                    <?php elseif ($lang == 'en'): ?>
                      Discussion
                    <?php endif; ?>
                  </button>
                </div>
                <div class="outro__social">
                  <button class="btn btn--social">
                    <img src="assets/svg/fb.svg" alt="">
                  </button>
                  <button class="btn btn--social">
                    <img src="assets/svg/twitter.svg" alt="">
                  </button>
                  <button class="btn btn--social">
                    <img src="assets/svg/gplus.svg" alt="">
                  </button>
                  <button class="btn btn--social">
                    <img src="assets/svg/link-tilted.svg" alt="">
                  </button>
                </div>
              </div>
            </div>
            <div class="outro__body">
              <div class="flex flex-wrap flex-auto no-shrink mxn1-5">
                <div class="col-md-3 px1-5">
                  <dl>
                    <?php if ( $lang == 'sk' ): ?>
                      <dt>Autori</dt>
                    <?php elseif ($lang == 'en'): ?>
                      <dt>Authors</dt>
                    <?php endif; ?>
                    <dd>Andrej Kuzmány</dd>
                    <dd>Michal Trško</dd>
                    <dd>Dávid Tvrdoň</dd>
                    <dd>Miloslav Šebela</dd>
                  </dl>
                </div>
                <div class="col-md-3 px1-5">
                  <dl>
                    <?php if ( $lang == 'sk' ): ?>
                      <dt>Produkcia a vývoj</dt>
                    <?php elseif ($lang == 'en'): ?>
                      <dt>Production and development</dt>
                    <?php endif; ?>
                    <dd>2FRESH SK</dd>
                  </dl>
                </div>
                <div class="col-md-3 px1-5">
                  <dl>
                    <?php if ( $lang == 'sk' ): ?>
                      <dt>Foto a video</dt>
                    <?php elseif ($lang == 'en'): ?>
                      <dt>Photo and video</dt>
                    <?php endif; ?>
                    <dd>SITA</dd>
                    <dd>TASR</dd>
                    <dd>PROFIMEDIA</dd>
                  </dl>
                </div>
              </div>
              <!-- <div class="interest-title">Mohlo by vás zaujímať</div> -->
              <!-- <div class="flex flex-wrap mxn1-5">
                <div class="col-md-6 px1-5">
                  <a href="#" class="interest-box mb3md">
                    <div class="interest-box__title">Ľudo Zúbek</div>
                    <div class="interest-box__text">
                      Naposledy na nej triumfoval Sagan. Prestížna klasika bude mať naďalej tradičný cieľ
                    </div>
                  </a>
                </div>
                <div class="col-md-6 px1-5">
                  <a href="#" class="interest-box mb3md">
                    <div class="interest-box__title">Pamela Anderson</div>
                    <div class="interest-box__text">Sagan: Dobre, že mi nechali miesto, lebo ja nebrzdím</div>
                  </a>
                </div>
              </div> -->
              <div class="bulk-box bulk-box--wide">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- adsence-sg-970x250 -->
                <ins class="adsbygoogle"
                     style="display:inline-block;width:970px;height:250px"
                     data-ad-client="ca-pub-6456364300478111"
                     data-ad-slot="4717613451"></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
                <?php if ( $lang == 'sk' ): ?>
                  <!-- sk ad -->
                <?php elseif ($lang == 'en'): ?>
                  <!-- en ad -->
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>


    </div>

    <video class="video" id="video" playsinline></video>
    <audio class="audio" id="audio"></audio>

    <script src="assets/js/vendors.min.js"></script>
    <script src="assets/js/app.min.js"></script>
  </body>
</html>
