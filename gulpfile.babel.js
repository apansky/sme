import gulp from 'gulp';
import babel from 'gulp-babel';
import sass from 'gulp-sass';
import concat from 'gulp-concat';
import autoprefixer from 'gulp-autoprefixer';
import uglify from 'gulp-uglify';
import rename from 'gulp-rename';
import cssnano from 'gulp-cssnano';
import sourcemaps from 'gulp-sourcemaps';
import plumber from 'gulp-plumber';

let vendors = [
  './bower_components/hammerjs/hammer.js',
  './bower_components/jquery/dist/jquery.min.js',
  './bower_components/progressbar.js/dist/progressbar.min.js'
];

gulp.task('css', () => {
  return gulp.src('src/scss/app.scss')
  //.pipe(sourcemaps.init())
  .pipe(plumber())
  .pipe(sass().on('error', sass.logError))
  .pipe(autoprefixer('last 5 version'))
  .pipe(cssnano())
  .pipe(rename({ suffix: '.min' }))
  //.pipe(sourcemaps.write())
  .pipe(gulp.dest('app/assets/css'));
});

gulp.task('js', () => {
  gulp.src('./src/js/*.js')
  .pipe(plumber())
  .pipe(concat('app.js'))
  // .pipe(sourcemaps.init())
  .pipe(babel({
    presets: ['es2015']
  }))
  .pipe(uglify())
  // .pipe(sourcemaps.write())
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest('app/assets/js'));
});

gulp.task('jsBuild', () => {
  gulp.src(vendors)
  .pipe(concat('vendors.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('app/assets/js'));
});

gulp.task('default', ['css', 'js'], () => {
  gulp.watch('src/scss/**', ['css']);
  gulp.watch('src/js/**', ['js']);
});
