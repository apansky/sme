
Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
  get: function() {
    return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
  }
});

const ios = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

if (ios) {
 $('body').addClass('ios');
}

const hammerConfig = ios ? {touchAction : 'auto'} : {};

const swipe = document.getElementById('swipe');
const mc = new Hammer(swipe, hammerConfig);

mc.on("swipeleft swiperight", function(ev) {
  const target = ev.target.className;
  if ( !(target.includes('js-prevent')) ) {
    if (ev.type === 'swipeleft') {
      navigate('next');
    }
    if (ev.type === 'swiperight') {
      navigate('prev');
    }
  }
});

function readDeviceOrientation() {
    switch (window.orientation) {
    case 0:
      $('body').removeClass('ios--landscape');
      $('body').addClass('ios--portrait');
      break;
    case 180:
    case -90:
    case 90:
      $('body').removeClass('ios--portrait');
      $('body').addClass('ios--landscape');
      break;
    }
}

readDeviceOrientation();

window.onorientationchange = readDeviceOrientation;

const playIcon = '<img src="assets/svg/play.svg">';
const pauseIcon = '<img src="assets/svg/pause.svg">';
const SLIDES_MIN_LIMIT = 1;
const SLIDES_MAX_LIMIT = 43;
let bar;

$(document).on('keydown', (e) => {
  if ( e.keyCode == 37 ) {
    navigate('prev');
  }
  if ( e.keyCode == 39 ) {
    navigate('next');
  }
});

function mediaEnded(slide) {
  $(`#${slide}`).find('.js-play-icon').attr('src', 'assets/svg/play.svg');
  bar.set(0);
}
function updateVideoProgressBar() {
  const percentage = (Math.floor((100 / $("#video")[0].duration) * $("#video")[0].currentTime)) / 100;
  bar.set(percentage);
}
function updateAudioProgressBar() {
  const percentage = (Math.floor((100 / $("#audio")[0].duration) * $("#audio")[0].currentTime)) / 100;
  bar.set(percentage);
}
function handleMedia(slide) {
  $("#video").attr('src', '');
  $("#audio").attr('src', '');

  if (bar) {
    bar.set(0);
  }

  const activeSlide = $('.story.is-active');
  const videoFolder = 'assets/video/';
  const audioFolder = 'assets/audio/';
  const videoPath = activeSlide.data('video');
  const audioPath = activeSlide.data('audio');

  if (videoPath || audioPath) {
    if (videoPath) {
      $('#video').attr('src', videoFolder + videoPath);
      $("#video")[0].play();

      $("#video")[0].addEventListener('timeupdate', updateVideoProgressBar, false);
      $("#video")[0].addEventListener('loadeddata', videoLoaded, false);
      $("#video")[0].addEventListener('ended', mediaEnded.bind(this, slide), false);
    }

    if (audioPath) {
      $('#audio').attr('src', audioFolder + audioPath);
      $("#audio")[0].play();

      $("#audio")[0].addEventListener('timeupdate', updateAudioProgressBar, false);
      $("#audio")[0].addEventListener('ended', mediaEnded.bind(this, slide), false);
    }

    const playerId = activeSlide.data('player');
    bar = new ProgressBar.Circle(`#player${playerId}`, {
      strokeWidth: 6,
      easing: 'ease',
      duration: 100,
      color: '#fff',
      trailColor: 'transparent',
      trailWidth: 0,
      svgStyle: null
    });
  }

  function videoLoaded() {
    activeSlide.css({'background': 'transparent', 'background-image': 'none'});
  }
}
function navigate(where) {
  const activeSlide = $('.story.is-active');
  const activeId = parseInt($('.story.is-active').attr('id'));
  let whereToGo;

  if ((where === 'prev') && (activeId > SLIDES_MIN_LIMIT)) {
    $('.nav').removeClass('nav--initial nav--outro');
    whereToGo = activeId - 1;
    activeSlide.removeClass('is-active');
    $(`#${whereToGo}`).addClass('is-active');
  }

  if ((where === 'next') && (activeId < SLIDES_MAX_LIMIT)) {
    $('.nav').removeClass('nav--initial nav--outro');
    whereToGo = activeId + 1;
    activeSlide.removeClass('is-active');
    $(`#${whereToGo}`).addClass('is-active');
  }

  if ($(`#${whereToGo}`).hasClass('story--intro')) {
    $('.nav').addClass('nav--initial');
  }

  if ($(`#${whereToGo}`).hasClass('story--outro')) {
    $('.nav').addClass('nav--outro');
  }

  handleMedia(whereToGo);
}

$('.js-prev').on('click', navigate.bind(this, 'prev'));
$('.js-next').on('click', navigate.bind(this, 'next'));


$('.js-begin').on('click', () => {
  const activeSlide = $('.story.is-active');

  $('.nav').removeClass('nav--initial');
  activeSlide.removeClass('is-active');
  $('#2').addClass('is-active');
  handleMedia(2);
});

$('.js-timeline').on('click', function() {
  const target = $(this).data('target');
  const parentId = $(this).closest('.story').attr('id');

  $(`#${parentId} .timeline-item`).removeClass('is-active');
  $(`#${parentId} .timeline__dot`).removeClass('is-active');

  $(`#${target}`).addClass('is-active');
  $(this).addClass('is-active');
});

$('.js-player').on('click', function() {
  const type = $(this).data('type');
  const player = $(`#${type}`);
  const icon = $(this).find('img');

  if (player[0].playing) {
    player[0].pause();
    icon.attr('src', 'assets/svg/play.svg');
  } else {
    player[0].play();
    icon.attr('src', 'assets/svg/pause.svg');
  }
});

$('.js-share').on('click', () => {
  event.stopPropagation();
  $('.share-box').toggleClass('is-active');
  $('.story.is-active .js-alpha').toggleClass('is-sharing');
});

$(window).on('click', () => {
  $('.share-box').removeClass('is-active');
  $('.story.is-active .js-alpha').removeClass('is-sharing');
});
